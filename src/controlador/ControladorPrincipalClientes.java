package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.consultas;
import vista.AgregarClientes;
import vista.IniciarSesion;
import vista.ModificarClientes;
import vista.PaginaPrincipal;
import vista.PrincipalClientes;
import vista.PrincipalEmpleados;
import vista.PrincipalFacturas;
import vista.PrincipalProductos;
import vista.PrincipalProveedores;
import vista.PuntoDeVenta;
import vista.ReporteDeVentas;

public class ControladorPrincipalClientes implements ActionListener{
private consultas modelo;
    private PrincipalClientes vista;
    private PrincipalEmpleados vistaempleados = new PrincipalEmpleados();
    private PrincipalClientes vistaclientes = new PrincipalClientes();
    private PrincipalFacturas vistafacturas = new PrincipalFacturas();
    private PrincipalProductos vistaproductos = new PrincipalProductos();
    private PrincipalProveedores vistaproveedores = new PrincipalProveedores();
    private PuntoDeVenta vistapuntodeventa = new PuntoDeVenta();
    private ReporteDeVentas vistareportesdeventa = new ReporteDeVentas();
    private IniciarSesion vistainiciarsesion = new IniciarSesion();
    private AgregarClientes vistaagregarclientes = new AgregarClientes();
    private ModificarClientes vistamodificarclientes = new ModificarClientes();
    
    
    public ControladorPrincipalClientes(consultas modelo, PrincipalClientes vista) {
        this.modelo = modelo;
        this.vista = vista;
        this.vista.btnClientes.addActionListener(this);
        this.vista.btnProductos.addActionListener(this);
        this.vista.btnProveedores.addActionListener(this);
        this.vista.btnPuntoVenta.addActionListener(this);
        this.vista.btnRecursosHumanos.addActionListener(this);
        this.vista.btnReportesVenta.addActionListener(this);
        this.vista.jButton7.addActionListener(this);
        this.vista.btnSalir.addActionListener(this);
        this.vista.agregarclientebtn.addActionListener(this);
        this.vista.modificarclientebtn.addActionListener(this);
        this.vista.limpiarclientesbtn.addActionListener(this);
        this.vista.todosclientesbtn.addActionListener(this);
        this.vista.buscarclientesbtn.addActionListener(this);
        this.vista.eliminarclientebtn.addActionListener(this);
    }
     public void iniciarVista() {
        vista.setTitle("PRINCIPAL CLIENTES");
        vista.pack();
        vista.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        vista.setLocationRelativeTo(vista);
        vista.setVisible(true);
    }
    public void limpiarCajasTexto(){
        vista.buscarclienteslbl.setText(" ");
        vista.clientestbt.setModel(new DefaultTableModel());
    }
     
    @Override
    public void actionPerformed(ActionEvent evento) {
               if(vista.eliminarclientebtn == evento.getSource()){
                    try{
                        modelo.Eliminar_Clientes(Integer.parseInt(vista.buscarclienteslbl.getText())); 
                        JOptionPane.showMessageDialog(null, "Registro eliminado exitosamente");
                    }catch(Exception e){
                        JOptionPane.showMessageDialog(null, "No se pudo eliminar el registro");
                    }
                }else if(vista.limpiarclientesbtn == evento.getSource()){//limpiar
                        limpiarCajasTexto();

               }else if(vista.buscarclientesbtn == evento.getSource()){//buscar especifico
                   this.vista.clientestbt.setModel(modelo.Consultar_Clientes_Especifico(Integer.parseInt(vista.buscarclienteslbl.getText()))); 
                   
               }else if(vista.todosclientesbtn == evento.getSource()) {//todos los clientes
                        modelo.Consultar_Clientes();
                        this.vista.clientestbt.setModel(modelo.Consultar_Clientes());   
                        
               }else if(vista.btnPuntoVenta == evento.getSource()){
                    try{
                        ControladorPuntoDeVenta cpe = new ControladorPuntoDeVenta(modelo, vistapuntodeventa);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if (vista.btnClientes == evento.getSource()){
                   try{
                        ControladorPrincipalClientes cpe = new ControladorPrincipalClientes(modelo, vistaclientes);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.btnProductos == evento.getSource()){
                   try{
                        ControladorPrincipalProductos cpe = new ControladorPrincipalProductos(modelo, vistaproductos);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               } else if(vista.btnProveedores == evento.getSource()){
                   try{
                        ControladorPrincipalProveedores cpe = new ControladorPrincipalProveedores(modelo, vistaproveedores);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    } 
               }else if(vista.btnRecursosHumanos == evento.getSource()){
                   try{
                        ControladorPrincipalEmpleados cpe = new ControladorPrincipalEmpleados(modelo, vistaempleados);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.jButton7 == evento.getSource()){
                   try{
                        ControladorPrincipalFacturas cpe = new ControladorPrincipalFacturas(modelo, vistafacturas);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.btnReportesVenta == evento.getSource()){
                   try{
                        ControladorReporteDeVenta cpe = new ControladorReporteDeVenta(modelo, vistareportesdeventa);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
              }else if (vista.btnSalir == evento.getSource()){
                   try{
                        ControladorIniciarSesion cpe = new ControladorIniciarSesion(modelo, vistainiciarsesion);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
                }else if (vista.agregarclientebtn == evento.getSource()){
                   try{
                        ControladorAgregarClientes cpe = new ControladorAgregarClientes(modelo, vistaagregarclientes);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
                }else if (vista.modificarclientebtn == evento.getSource()){
                   try{
                        ControladorModificarClientes cpe = new ControladorModificarClientes(modelo, vistamodificarclientes);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
                }
        
    }

    
    
}
