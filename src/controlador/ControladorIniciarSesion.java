/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelo.consultas;
import vista.AgregarClientes;
import vista.IniciarSesion;
import vista.PaginaPrincipal;

public class ControladorIniciarSesion implements ActionListener{
    private consultas modelo;
    private IniciarSesion vista;
    private PaginaPrincipal vistapaginaprincipal = new PaginaPrincipal();

    public ControladorIniciarSesion(consultas modelo, IniciarSesion vista) {
        this.modelo = modelo;
        this.vista = vista;
        this.vista.btnConectar.addActionListener(this);
    }

     public void iniciarVista() {
        vista.setTitle("INICIAR SESIÓN");
        vista.pack();
        vista.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        vista.setLocationRelativeTo(vista);
        vista.setVisible(true);
    }
    @Override
    public void actionPerformed(ActionEvent evento) {
        if(vista.btnConectar == evento.getSource()) {
           
               try{
                ControladorPaginaPrincipal cpprin = new ControladorPaginaPrincipal(modelo, vistapaginaprincipal);
                cpprin.iniciarVista();
                this.vista.setVisible(false);
            }catch(Exception e){
                System.out.println("ERROR"+e);  
            }
        }else{
            
        }
    }
}

