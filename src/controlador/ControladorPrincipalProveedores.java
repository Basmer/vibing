/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.consultas;
import vista.AgregarProveedores;
import vista.IniciarSesion;
import vista.ModificarProveedores;
import vista.PrincipalClientes;
import vista.PrincipalEmpleados;
import vista.PrincipalFacturas;
import vista.PrincipalProductos;
import vista.PrincipalProveedores;
import vista.PuntoDeVenta;
import vista.ReporteDeVentas;

public class ControladorPrincipalProveedores implements ActionListener{
    private consultas modelo;
    private PrincipalProveedores vista;
    private PrincipalEmpleados vistaempleados = new PrincipalEmpleados();
    private PrincipalClientes vistaclientes = new PrincipalClientes();
    private PrincipalFacturas vistafacturas = new PrincipalFacturas();
    private PrincipalProductos vistaproductos = new PrincipalProductos();
    private PrincipalProveedores vistaproveedores = new PrincipalProveedores();
    private PuntoDeVenta vistapuntodeventa = new PuntoDeVenta();
    private ReporteDeVentas vistareportesdeventa = new ReporteDeVentas();
    private IniciarSesion vistainiciarsesion = new IniciarSesion();
    private AgregarProveedores vistaagregarproveedores= new AgregarProveedores();
    private ModificarProveedores vistamodificarproveedores = new ModificarProveedores();

    public ControladorPrincipalProveedores(consultas modelo, PrincipalProveedores vista) {
        this.modelo = modelo;
        this.vista = vista;
        this.vista.btnClientes.addActionListener(this);
        this.vista.btnProductos.addActionListener(this);
        this.vista.btnProveedores.addActionListener(this);
        this.vista.btnPuntoVenta.addActionListener(this);
        this.vista.btnRecursosHumanos.addActionListener(this);
        this.vista.btnReportesVenta.addActionListener(this);
        this.vista.jButton7.addActionListener(this);
        this.vista.btnSalir1.addActionListener(this);
        this.vista.agregarproveedoresbtn.addActionListener(this);
        this.vista.modificarproveedoresbtn.addActionListener(this);
        this.vista.todosproveedoresbtn.addActionListener(this);
        this.vista.limpiarproveedoresbtn.addActionListener(this);
        this.vista.buscarproveedoresbtn.addActionListener(this);
        this.vista.eliminarproveedoresbtn.addActionListener(this);

    }
    public void iniciarVista() {
        vista.setTitle("PRINCIPAL PROVEEDORES");
        vista.pack();
        vista.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        vista.setLocationRelativeTo(vista);
        vista.setVisible(true);
    }
    public void limpiarCajasTexto(){
        vista.buscarproveedoreslbl.setText("");
        vista.proveedorestbt.setModel(new DefaultTableModel());
    }
    @Override
    public void actionPerformed(ActionEvent evento) {
                if(vista.eliminarproveedoresbtn == evento.getSource()){
                    try{
                        modelo.Eliminar_Proveedores(Integer.parseInt(vista.buscarproveedoreslbl.getText())); 
                        JOptionPane.showMessageDialog(null, "Registro eliminado exitosamente");
                    }catch(Exception e){
                        JOptionPane.showMessageDialog(null, "No se pudo eliminar el registro");
                    }
                }else if(vista.limpiarproveedoresbtn == evento.getSource()){//limpiar
                        limpiarCajasTexto();

               }else if(vista.todosproveedoresbtn == evento.getSource()){//todos
                    modelo.Consultar_Proveedores();
                    this.vista.proveedorestbt.setModel(modelo.Consultar_Proveedores());
                    
               }else if(vista.buscarproveedoresbtn == evento.getSource()){//buscar especifico
                   this.vista.proveedorestbt.setModel(modelo.Consultar_Proveedores_Especifico(Integer.parseInt(vista.buscarproveedoreslbl.getText()))); 
                   
               }else if (vista.btnClientes == evento.getSource()){
                   try{
                        ControladorPrincipalClientes cpe = new ControladorPrincipalClientes(modelo, vistaclientes);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.btnProductos == evento.getSource()){
                   try{
                        ControladorPrincipalProductos cpe = new ControladorPrincipalProductos(modelo, vistaproductos);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               } else if(vista.btnProveedores == evento.getSource()){
                   try{
                        ControladorPrincipalProveedores cpe = new ControladorPrincipalProveedores(modelo, vistaproveedores);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    } 
               }else if(vista.btnRecursosHumanos == evento.getSource()){
                   try{
                        ControladorPrincipalEmpleados cpe = new ControladorPrincipalEmpleados(modelo, vistaempleados);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.jButton7 == evento.getSource()){
                   try{
                        ControladorPrincipalFacturas cpe = new ControladorPrincipalFacturas(modelo, vistafacturas);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.btnReportesVenta == evento.getSource()){
                   try{
                        ControladorReporteDeVenta cpe = new ControladorReporteDeVenta(modelo, vistareportesdeventa);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if (vista.btnSalir1 == evento.getSource()){
                   try{
                        ControladorIniciarSesion cpe = new ControladorIniciarSesion(modelo, vistainiciarsesion);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
                }else if (vista.agregarproveedoresbtn == evento.getSource()){
                   try{
                        ControladorAgregarProveedores cpe = new ControladorAgregarProveedores(modelo, vistaagregarproveedores);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
                }else if (vista.modificarproveedoresbtn == evento.getSource()){
                   try{
                        ControladorModificarProveedores cpe = new ControladorModificarProveedores(modelo, vistamodificarproveedores);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
                }
    }
    
}
