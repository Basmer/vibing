package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.consultas;
import vista.AgregarEmpleados;
import vista.IniciarSesion;
import vista.ModificarEmpleados;
import vista.PrincipalClientes;
import vista.PrincipalEmpleados;
import vista.PrincipalFacturas;
import vista.PrincipalProductos;
import vista.PrincipalProveedores;
import vista.PuntoDeVenta;
import vista.ReporteDeVentas;


public class ControladorPrincipalEmpleados implements ActionListener{
    private consultas modelo;
    private PrincipalEmpleados vista;
    private PrincipalEmpleados vistaempleados = new PrincipalEmpleados();
    private PrincipalClientes vistaclientes = new PrincipalClientes();
    private PrincipalFacturas vistafacturas = new PrincipalFacturas();
    private PrincipalProductos vistaproductos = new PrincipalProductos();
    private PrincipalProveedores vistaproveedores = new PrincipalProveedores();
    private PuntoDeVenta vistapuntodeventa = new PuntoDeVenta();
    private ReporteDeVentas vistareportesdeventa = new ReporteDeVentas();
    private IniciarSesion vistainiciarsesion = new IniciarSesion();
    private AgregarEmpleados vistaagregarempleados = new AgregarEmpleados();
    private ModificarEmpleados vistamodificarempleados = new ModificarEmpleados();
    

    public ControladorPrincipalEmpleados(consultas modelo, PrincipalEmpleados vista) {
        this.modelo = modelo;
        this.vista = vista;
        this.vista.todosempleadosbtn.addActionListener(this);
        this.vista.btnClientes.addActionListener(this);
        this.vista.btnProductos2.addActionListener(this);
        this.vista.btnProveedores2.addActionListener(this);
        this.vista.btnPuntoVenta2.addActionListener(this);
        this.vista.btnRecursosHumanos.addActionListener(this);
        this.vista.btnReportesVenta.addActionListener(this);
        this.vista.jButton7.addActionListener(this);
        this.vista.btnSalir1.addActionListener(this);
        this.vista.agregarempleadosbtn.addActionListener(this);
        this.vista.modificarempleadosbtn.addActionListener(this);
        this.vista.limpiarempleadosbtn.addActionListener(this);
        this.vista.buscarempleadosbtn.addActionListener(this);
        this.vista.eliminarempleadosbtn.addActionListener(this);
        
        
    }

    
    public void iniciarVista() {
        vista.setTitle("PRINCIPAL EMPLEADOS");
        vista.pack();
        vista.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        vista.setLocationRelativeTo(vista);
        vista.setVisible(true);
    }
    
    public void limpiarCajasTexto(){
        vista.buscarempleadoslbl.setText("");
        vista.empleadostbt.setModel(new DefaultTableModel());
    }
    @Override
    public void actionPerformed(ActionEvent evento) {
                
                if(vista.eliminarempleadosbtn == evento.getSource()){
                    try{
                        modelo.Eliminar_Empleados(Integer.parseInt(vista.buscarempleadoslbl.getText())); 
                        JOptionPane.showMessageDialog(null, "Registro eliminado exitosamente");
                    }catch(Exception e){
                        JOptionPane.showMessageDialog(null, "No se pudo eliminar el registro");
                    }
                }else if(vista.todosempleadosbtn == evento.getSource()) {
                        modelo.Consultar_Empleados();
                        this.vista.empleadostbt.setModel(modelo.Consultar_Empleados());   
                        
               }else if(vista.limpiarempleadosbtn == evento.getSource()){//limpiar
                        limpiarCajasTexto();
            
               }else if(vista.buscarempleadosbtn == evento.getSource()){
                   this.vista.empleadostbt.setModel(modelo.Consultar_Empleados_Especifico(Integer.parseInt(vista.buscarempleadoslbl.getText()))); 
               }else if(vista.btnPuntoVenta2 == evento.getSource()){///menu   
                    try{
                        ControladorPuntoDeVenta cpe = new ControladorPuntoDeVenta(modelo, vistapuntodeventa);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if (vista.btnClientes == evento.getSource()){
                   try{
                        ControladorPrincipalClientes cpe = new ControladorPrincipalClientes(modelo, vistaclientes);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.btnProductos2 == evento.getSource()){
                   try{
                        ControladorPrincipalProductos cpe = new ControladorPrincipalProductos(modelo, vistaproductos);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               } else if(vista.btnProveedores2 == evento.getSource()){
                   try{
                        ControladorPrincipalProveedores cpe = new ControladorPrincipalProveedores(modelo, vistaproveedores);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    } 
               }else if(vista.btnRecursosHumanos == evento.getSource()){
                   try{
                        ControladorPrincipalEmpleados cpe = new ControladorPrincipalEmpleados(modelo, vistaempleados);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.jButton7 == evento.getSource()){
                   try{
                        ControladorPrincipalFacturas cpe = new ControladorPrincipalFacturas(modelo, vistafacturas);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.btnReportesVenta == evento.getSource()){
                   try{
                        ControladorReporteDeVenta cpe = new ControladorReporteDeVenta(modelo, vistareportesdeventa);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
              }else if (vista.btnSalir1 == evento.getSource()){
                   try{
                        ControladorIniciarSesion cpe = new ControladorIniciarSesion(modelo, vistainiciarsesion);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
                }else if (vista.agregarempleadosbtn == evento.getSource()){
                   try{
                        ControladorAgregarEmpleados cpe = new ControladorAgregarEmpleados(modelo, vistaagregarempleados);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
                }else if (vista.modificarempleadosbtn == evento.getSource()){
                   try{
                        ControladorModificarEmpleados cpe = new ControladorModificarEmpleados(modelo, vistamodificarempleados);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
                }
    }
    
    
}
