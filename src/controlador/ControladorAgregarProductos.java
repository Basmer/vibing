/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelo.consultas;
import vista.AgregarProductos;
import vista.IniciarSesion;
import vista.PaginaPrincipal;
import vista.PrincipalClientes;
import vista.PrincipalEmpleados;
import vista.PrincipalFacturas;
import vista.PrincipalProductos;
import vista.PrincipalProveedores;
import vista.PuntoDeVenta;
import vista.ReporteDeVentas;

public class ControladorAgregarProductos implements ActionListener{
     private consultas modelo;
    private AgregarProductos vista;
    private PrincipalEmpleados vistaempleados = new PrincipalEmpleados();
    private PrincipalClientes vistaclientes = new PrincipalClientes();
    private PrincipalFacturas vistafacturas = new PrincipalFacturas();
    private PrincipalProductos vistaproductos = new PrincipalProductos();
    private PrincipalProveedores vistaproveedores = new PrincipalProveedores();
    private PuntoDeVenta vistapuntodeventa = new PuntoDeVenta();
    private ReporteDeVentas vistareportesdeventa = new ReporteDeVentas();
    private IniciarSesion vistainiciarsesion = new IniciarSesion();
    public int cat;
    public int prov;
    public int ta;
    public int su;

    public ControladorAgregarProductos(consultas modelo, AgregarProductos vista) {
        this.modelo = modelo;
        this.vista = vista;
        this.vista.btnClientes.addActionListener(this);
        this.vista.btnProductos.addActionListener(this);
        this.vista.btnProveedores.addActionListener(this);
        this.vista.btnPuntoVenta.addActionListener(this);
        this.vista.btnRecursosHumanos.addActionListener(this);
        this.vista.btnReportesVenta.addActionListener(this);
        this.vista.jButton7.addActionListener(this);
        this.vista.btnSalir.addActionListener(this);
        this.vista.btnguardar.addActionListener(this);
    }
     public void iniciarVista() {
        vista.setTitle("AGREGAR PRODUCTOS");
        vista.pack();
        vista.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        vista.setLocationRelativeTo(vista);
        vista.setVisible(true);
    }
     public void categoria(){
         if ( vista.cbcategoria.getSelectedItem()== "Bebés"){
             cat=1;
         }else if(vista.cbcategoria.getSelectedItem()== "Niños"){
             cat=2;
         }else if(vista.cbcategoria.getSelectedItem()== "Damas"){
             cat=3;
         }else if(vista.cbcategoria.getSelectedItem()== "Caballero"){
             cat=4;
         }else if(vista.cbcategoria.getSelectedItem()== "Accesorios Dama"){
             cat=5;
         }else{
             cat=6;
         }  
     }
     public void proveedores(){
         if ( vista.cbproveedor.getSelectedItem()== "Moda Suprema"){
             prov=1;
         }else if(vista.cbproveedor.getSelectedItem()== "Just Clothes"){
             prov=2;
         }else if(vista.cbproveedor.getSelectedItem()== "Kids a la moda"){
             prov=3;
         }else if(vista.cbproveedor.getSelectedItem()== "Loyalty Clothes"){
             prov=4;
         }else{
             prov=5;
         }  
     }
     
 public void talla(){
         if ( vista.cbTalla.getSelectedItem()== "XCH"){
             ta=1;
         }else if(vista.cbTalla.getSelectedItem()== "CH"){
             ta=2;
         }else if(vista.cbTalla.getSelectedItem()== "M"){
             ta=3;
         }else if(vista.cbTalla.getSelectedItem()== "G"){
             ta=4;
         }else if(vista.cbTalla.getSelectedItem()== "XG"){
             ta=5;
         }else{
             ta=6;
         }  
     }
     public void sucursal(){
         if ( vista.cbsucursal.getSelectedItem()== "Sucursal Centro"){
             su=1;
         }else if(vista.cbsucursal.getSelectedItem()== "Sucursal Gran Plaza"){
             su=2;
         }else{
             su=3;
         }  
     }
    @Override
    public void actionPerformed(ActionEvent evento){
        if(vista.btnguardar == evento.getSource()) {
                try{
                    categoria();
                    talla();
                    proveedores();
                    sucursal();
                    modelo.Insertar_Productos(Integer.parseInt(vista.txtID.getText()), vista.txtNombre.getText(), vista.txtDescripcion.getText(),Float.parseFloat(vista.txtPrecio.getText()), prov,cat);
                    modelo.Insertar_Sucursal_Inventario(su, Integer.parseInt(vista.txtExistencia.getText()),Integer.parseInt(vista.txtID.getText()),ta );
                    JOptionPane.showMessageDialog(null, "Registro insertado exitosamente");
                }catch(Exception e){
                    System.out.println("Error:"+e);
                    JOptionPane.showMessageDialog(null, "No se pudo insertar");
                }
            }
    else if(vista.btnPuntoVenta == evento.getSource()){
                    try{
                        ControladorPuntoDeVenta cpe = new ControladorPuntoDeVenta(modelo, vistapuntodeventa);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if (vista.btnClientes == evento.getSource()){
                   try{
                        ControladorPrincipalClientes cpe = new ControladorPrincipalClientes(modelo, vistaclientes);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.btnProductos == evento.getSource()){
                   try{
                        ControladorPrincipalProductos cpe = new ControladorPrincipalProductos(modelo, vistaproductos);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               } else if(vista.btnProveedores == evento.getSource()){
                   try{
                        ControladorPrincipalProveedores cpe = new ControladorPrincipalProveedores(modelo, vistaproveedores);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    } 
               }else if(vista.btnRecursosHumanos == evento.getSource()){
                   try{
                        ControladorPrincipalEmpleados cpe = new ControladorPrincipalEmpleados(modelo, vistaempleados);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.jButton7 == evento.getSource()){
                   try{
                        ControladorPrincipalFacturas cpe = new ControladorPrincipalFacturas(modelo, vistafacturas);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
               }else if(vista.btnReportesVenta == evento.getSource()){
                   try{
                        ControladorReporteDeVenta cpe = new ControladorReporteDeVenta(modelo, vistareportesdeventa);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
        }else if (vista.btnSalir == evento.getSource()){
                   try{
                        ControladorIniciarSesion cpe = new ControladorIniciarSesion(modelo, vistainiciarsesion);
                        cpe.iniciarVista();
                        this.vista.setVisible(false);
                    }catch(Exception e){
                        System.out.println("ERROR"+e);  
                    }
                }
    }
    
    
}
